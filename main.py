from os import system as runcmd
import os
dirPath = os.path.dirname(os.path.realpath(__file__))
import sys
from platform import system as currentOS
if currentOS() == "Darwin":
    runcmd('/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"')
    runcmd("brew install wget")
    runcmd("rm " + dirPath + "/get-pip.py")
    runcmd("wget https://bootstrap.pypa.io/get-pip.py")
    runcmd("python3 " + dirPath + "/get-pip.py")
elif currentOS() == "Linux":
    runcmd("sudo apt-get install wget")
    runcmd("rm " + dirPath + "/get-pip.py")
    runcmd("wget https://bootstrap.pypa.io/get-pip.py")
    runcmd("python3 " + dirPath + "/get-pip.py")
elif currentOS() == "Windows":
    runcmd("rm " + dirPath + "/get-pip.py")
    runcmd(dirPath + "/wget " + "https://bootstrap.pypa.io/get-pip.py")
    runcmd("python3 " + dirPath + "/get-pip.py")
from termcolor import colored as ct
runcmd("clear")
print("Please enter the "+ ct("name", "red") +" of the file (in the input folder)!")
fileToReadName = input(ct("- ", "red"))
fileToRead = open(dirPath + "/input/" + fileToReadName, "r")
currentLine = None
outputFile = open(dirPath + "/output/output.php", "w")
outputFile.write("<?php\n")
while currentLine != "":
    currentLine = fileToRead.readline()
    if "print" in currentLine:
        bracketPos = currentLine.find("(")
        bracketPos2 = currentLine.find(")")
        textToPrintIn = currentLine[bracketPos:bracketPos2]
        outputFile.write("echo " + textToPrintIn + ";")
    elif "import" in currentLine:
        currentLine.split()
        packageToImport = currentLine[2]
        if currentOS() == "Darwin":
            runcmd('wget ' + "https://pypi.org/project/" + packageToImport + "/#files")
        elif currentOS() == "Linux":
            runcmd("rm " + dirPath + "/index.html")
            runcmd('wget ' + "https://pypi.org/project/" + packageToImport + "/#files")
        elif currentOS() == "Windows":
            runcmd("rm " + dirPath + "/index.html")
            runcmd(dirPath + "/wget " + "https://pypi.org/project/" + packageToImport + "/#files")
        packageWebPage = open(dirPath + "/index.html")
        packageWebPageHTML = packageWebPage.read()
        spanFileNameAndSize = packageWebPageHTML.find('<span class=table__mobile-label>Filename, size</span>')
        print(spanFileNameAndSize)
        #-1
        #19951
        #-1
        #-1
        spanFileType = packageWebPageHTML.find('<span class="table__mobile-label">File type</span>')
        print(spanFileType)
        textBetweenTwoPoints = packageWebPageHTML[spanFileNameAndSize:spanFileType]
        print(textBetweenTwoPoints)
        hrefDefPos = textBetweenTwoPoints.find('<a href="')
        print(hrefDefPos)
        linkTextEnd = textBetweenTwoPoints.find('">')
        print(linkTextEnd)
        tarUrl = textBetweenTwoPoints[hrefDefPos:linkTextEnd]
        try:
            print(tarUrl)
        except:
            print(ct("ERROR: Something's stupid! If the problem persists, report it in the form of a git issue!", "red"))
fileToRead.close()
outputFile.write("?>")
outputFile.close()
